#ifndef BLDC_DRIVER_H
#define BLDC_DRIVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/dma.h"

enum pins
{
    EN_1_PIN = 10,
    EN_2_PIN = 11,
    EN_3_PIN = 12,
    PHASE_1_PIN = 13,
    PHASE_2_PIN = 14,
    PHASE_3_PIN = 15
};

void init_pins();

void enable_driver();

void disable_driver();

void start_motor();

void stop_motor();

#endif