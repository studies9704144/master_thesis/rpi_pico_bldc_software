#include "bldc_driver.h"

int main()
{
    const uint LED_PIN = PICO_DEFAULT_LED_PIN;
    stdio_init_all();
    setup_default_uart();

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    init_pins();
    enable_driver();

    printf("Raspberry Pi Pico - works. Curr time: %.2f [s]\n", time_us_32() / 1000000);
    gpio_put(LED_PIN, 1);
    sleep_ms(500);

    while (1)
    {
        start_motor();
    }
    return 0;
}