#include "bldc_driver.h"

#define HIGH_STATE_TIME 10
#define LOW_STATE_TIME 50

void init_pins()
{
    gpio_init(EN_1_PIN);
    gpio_init(EN_2_PIN);
    gpio_init(EN_3_PIN);
    gpio_init(PHASE_1_PIN);
    gpio_init(PHASE_2_PIN);
    gpio_init(PHASE_3_PIN);

    gpio_set_dir(EN_1_PIN, GPIO_OUT);
    gpio_set_dir(EN_2_PIN, GPIO_OUT);
    gpio_set_dir(EN_3_PIN, GPIO_OUT);
    gpio_set_dir(PHASE_1_PIN, GPIO_OUT);
    gpio_set_dir(PHASE_2_PIN, GPIO_OUT);
    gpio_set_dir(PHASE_3_PIN, GPIO_OUT);

    gpio_clr_mask((1 << EN_1_PIN) | (1 << EN_2_PIN) | (1 << EN_3_PIN) | (1 << PHASE_1_PIN) | (1 << PHASE_2_PIN) | (1 << PHASE_3_PIN));
}

void enable_driver()
{
    gpio_put(EN_1_PIN, true);
    gpio_put(EN_2_PIN, true);
    gpio_put(EN_3_PIN, true);
}

void disable_driver()
{
    gpio_put(EN_1_PIN, false);
    gpio_put(EN_2_PIN, false);
    gpio_put(EN_3_PIN, false);
}

void start_motor()
{
    for (int i = 0; i < 23; i++)
    {
        gpio_set_mask((1 << PHASE_1_PIN));
        sleep_us(HIGH_STATE_TIME);
        gpio_clr_mask((1 << PHASE_1_PIN));
        sleep_us(LOW_STATE_TIME);
    }
    gpio_set_mask((1 << PHASE_1_PIN));
    sleep_us(HIGH_STATE_TIME);
    gpio_clr_mask((1 << PHASE_1_PIN));

    for (int i = 0; i < 23; i++)
    {
        gpio_set_mask((1 << PHASE_2_PIN));
        sleep_us(HIGH_STATE_TIME);
        gpio_clr_mask((1 << PHASE_2_PIN));
        sleep_us(LOW_STATE_TIME);
    }
    gpio_set_mask((1 << PHASE_2_PIN));
    sleep_us(HIGH_STATE_TIME);
    gpio_clr_mask((1 << PHASE_2_PIN));

    for (int i = 0; i < 23; i++)
    {
        gpio_set_mask((1 << PHASE_3_PIN));
        sleep_us(HIGH_STATE_TIME);
        gpio_clr_mask((1 << PHASE_3_PIN));
        sleep_us(LOW_STATE_TIME);
    }
    gpio_set_mask((1 << PHASE_3_PIN));
    sleep_us(HIGH_STATE_TIME);
    gpio_clr_mask((1 << PHASE_3_PIN));
}

void stop_motor()
{
    gpio_clr_mask((1 << PHASE_1_PIN) | (1 << PHASE_2_PIN) | (1 << PHASE_3_PIN));
}